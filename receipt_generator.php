<?php declare(strict_types = 1);

require __DIR__ . '/vendor/autoload.php';

class PDF extends FPDF
{
    public function title(string $title = ''): void
    {
        $this->SetFont('Arial', '', 16);
        $this->Cell(0, 10, $title, 0, 0, 'C');
        $this->Ln(20);
    }

    public function setCMargin(int $margin = 0): void
    {
        $this->cMargin = $margin;
    }
    
    // To add FJ = Force Justify http://www.fpdf.org/en/script/script8.php
    public function Cell($w, $h=0, $txt='', $border=0, $ln=0, $align='', $fill=false, $link='')
    {
        $k=$this->k;
        if($this->y+$h>$this->PageBreakTrigger && !$this->InHeader && !$this->InFooter && $this->AcceptPageBreak())
        {
            $x=$this->x;
            $ws=$this->ws;
            if($ws>0)
            {
                $this->ws=0;
                $this->_out('0 Tw');
            }
            $this->AddPage($this->CurOrientation);
            $this->x=$x;
            if($ws>0)
            {
                $this->ws=$ws;
                $this->_out(sprintf('%.3F Tw',$ws*$k));
            }
        }
        if($w==0)
            $w=$this->w-$this->rMargin-$this->x;
        $s='';
        if($fill || $border==1)
        {
            if($fill)
                $op=($border==1) ? 'B' : 'f';
            else
                $op='S';
            $s=sprintf('%.2F %.2F %.2F %.2F re %s ',$this->x*$k,($this->h-$this->y)*$k,$w*$k,-$h*$k,$op);
        }
        if(is_string($border))
        {
            $x=$this->x;
            $y=$this->y;
            if(is_int(strpos($border,'L')))
                $s.=sprintf('%.2F %.2F m %.2F %.2F l S ',$x*$k,($this->h-$y)*$k,$x*$k,($this->h-($y+$h))*$k);
            if(is_int(strpos($border,'T')))
                $s.=sprintf('%.2F %.2F m %.2F %.2F l S ',$x*$k,($this->h-$y)*$k,($x+$w)*$k,($this->h-$y)*$k);
            if(is_int(strpos($border,'R')))
                $s.=sprintf('%.2F %.2F m %.2F %.2F l S ',($x+$w)*$k,($this->h-$y)*$k,($x+$w)*$k,($this->h-($y+$h))*$k);
            if(is_int(strpos($border,'B')))
                $s.=sprintf('%.2F %.2F m %.2F %.2F l S ',$x*$k,($this->h-($y+$h))*$k,($x+$w)*$k,($this->h-($y+$h))*$k);
        }
        if($txt!='')
        {
            if($align=='R')
                $dx=$w-$this->cMargin-$this->GetStringWidth($txt);
            elseif($align=='C')
                $dx=($w-$this->GetStringWidth($txt))/2;
            elseif($align=='FJ')
            {
                //Set word spacing
                $wmax=($w-2*$this->cMargin);
                $this->ws=($wmax-$this->GetStringWidth($txt))/substr_count($txt,' ');
                $this->_out(sprintf('%.3F Tw',$this->ws*$this->k));
                $dx=$this->cMargin;
            }
            else
                $dx=$this->cMargin;
            $txt=str_replace(')','\\)',str_replace('(','\\(',str_replace('\\','\\\\',$txt)));
            if($this->ColorFlag)
                $s.='q '.$this->TextColor.' ';
            $s.=sprintf('BT %.2F %.2F Td (%s) Tj ET',($this->x+$dx)*$k,($this->h-($this->y+.5*$h+.3*$this->FontSize))*$k,$txt);
            if($this->underline)
                $s.=' '.$this->_dounderline($this->x+$dx,$this->y+.5*$h+.3*$this->FontSize,$txt);
            if($this->ColorFlag)
                $s.=' Q';
            if($link)
            {
                if($align=='FJ')
                    $wlink=$wmax;
                else
                    $wlink=$this->GetStringWidth($txt);
                $this->Link($this->x+$dx,$this->y+.5*$h-.5*$this->FontSize,$wlink,$this->FontSize,$link);
            }
        }
        if($s)
            $this->_out($s);
        if($align=='FJ')
        {
            //Remove word spacing
            $this->_out('0 Tw');
            $this->ws=0;
        }
        $this->lasth=$h;
        if($ln>0)
        {
            $this->y+=$h;
            if($ln==1)
                $this->x=$this->lMargin;
        }
        else
            $this->x+=$w;
    }

    // UcWords with exceptions
    public function ucwords(string $str, array $exceptions): string {
        $out = "";
        foreach (explode(" ", $str) as $key => $word) {
            $out .= (!in_array($word, $exceptions) || $key == 0) ?
                 strtoupper($word{0}) . substr($word, 1) . " " : $word . " ";
        }
        return rtrim($out);
    }
}

$pdf = new PDF('P', 'mm', 'A4');
$pdf->AddPage();
$pdf->title('RECIBO');
$pdf->setCMargin(0);
$pdf->SetFont('Arial', '', 14);

$firstMonthDay = date('d/m/Y', strtotime('first day of this month'));
$lastMonthTz = strtotime('last day of this month');
$lastMonthDay = date('d/m/Y', $lastMonthTz);
setlocale(LC_TIME, 'pt_BR.UTF-8');
$lastMonthDayDT = $pdf->ucwords(
    utf8_decode(strftime('%d de %B de %Y')),
    ['de']
);

$inifile = 'example.ini';
if (file_exists('personalinfo.ini')) {
    $inifile = 'personalinfo.ini';
}
$personalInfo = parse_ini_file($inifile);

$content = <<<TEXT
Eu, $personalInfo[name], inscrito no CPF de número $personalInfo[cpf], declaro ter recebido a quantia de R$ $personalInfo[amount] de $personalInfo[company], sito na $personalInfo[company_address],
CEP $personalInfo[company_cep] e de CNPJ $personalInfo[company_cnpj] referentes a serviços de consultoria em informática prestados durante o período de $firstMonthDay a $lastMonthDay.
TEXT;

$pdf->MultiCell(
    190,
    8,
    utf8_decode($content),
    0,
    'FJ',
    0
);

$pdf->Cell(10, 100, "{$personalInfo['city']}, {$lastMonthDayDT}.");

if (isset($personalInfo['signature_img_path']) &&
    $personalInfo['signature_img_path'] === ''
) {
    $pdf->Line(70, 150, 150, 150);
}

if (isset($personalInfo['signature_img_path']) &&
    $personalInfo['signature_img_path'] !== ''
) {
    $pdf->Image(
        $personalInfo['signature_img_path'],
        50,
        135
    );
}

if ((int) $personalInfo['signature_bottom_name'] === 1) {
    $pdf->Cell(0, 200, $personalInfo['name'], 0, 0, 'C');
}

$pdf->Output('F', 'receipt.pdf');
