# receiptgenerator

Generates pdf generic receipts

## How to Use

- Add a ini file called personalinfo.ini (see the example.ini) to change the receipt content;
- Run the php file locally into your machine. Will be generated a receipt.pdf file:
`php receipt_generator.php && xdg-open receipt.pdf`
- Any problems of formatation you can edit the php file that uses FPDF package to generate the PDF;